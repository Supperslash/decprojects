# December projects   
### Hrodebert — 12/01/2023 1:01 AM   
I built a counter strike bomb replica as my second project.
-hardness to build: 1/5 very simple yet fun
-what I have lernt 4/5 I almost used every competent really cool    
   
  ![IMG\_2010.mov](files\img_2010.mov)    
 --- 
   
### Snwspeckle — 12/01/2023 2:29 PM   
The last few months I’ve been building an ESP32-based “That Was Easy” button and finally finished the project this week. If you’re curious how I built it and how to build one yourself, I just published the blog post of it last night.   
   
![image.png](files\image.png)    
   
### Kanken6174 — 12/02/2023 12:11 PM   
finished the tasked audio driver (phone microphone is quite directional, it sounds perfect IRL)   
   
[VID\_20231202\_190908.mp4](files\vid_20231202_190908.mp4)    
   
### MR.cumfart — 12/02/2023 3:16 PM
User made there build on the webpage:   
[https://medium.com/@0x\_liam/turn-off-any-tv-with-arduino-8463e67e1ef5](https://medium.com/@0x_liam/turn-off-any-tv-with-arduino-8463e67e1ef5)   
![image.png](files\image_h.png)    
### Bobski — 12/04/2023 1:03 AM

   
![image.png](files\image_2.png)    
   
### kevin — 12/04/2023 1:06 AM YT VIDEO!   
Finally I finished my project !
Microgenius v1.0 my pocket pc, made with arduino pro mini.
Watch full video on youtube :- [https://www.youtube.com/watch?v=nCn74JKpy5o&t=125s](https://www.youtube.com/watch?v=nCn74JKpy5o&t=125s)   
![image.png](files\image_j.png)    
   
### sajosup — 12/07/2023 8:56 PM   
4 gas detector i made   
[20231207\_210219.mp4](files\20231207_210219.mp4)    
   
### bvtv — 12/12/2023 1:19 AM YT video   

I’ve found my new favorite way to communicate with the arduino: In this video, I’m using modbus to communicate with the arduino through the USB Cable. I’ve made a gui program for the modbus client on the computer. What do you think? Arduino data to csv file using Modbus over usb (Modbus RTU)
[https://youtu.be/eN8-A4el9kk](https://youtu.be/eN8-A4el9kk)   
![maxresdefault.jpg](files\maxresdefault.jpg)    
   
### Marxu — 12/13/2023 1:36 AM   
Advanced my model rocket   
![image.png](files\image_x.png)    
   
### simobomb64 — 12/13/2023 2:21 PM   
an  animatronic using a breadboard and some buttons that are connected to an arduino   
[Boingo\_fivenightsatfreddys\_fnaf\_animatronics\_springbonnie\_animatronic.mp4](files\boingo_fivenightsatfreddys_fnaf_animatronics_spr.mp4)    
   
   
### Dönermann — 12/14/2023 12:43 PM   
it works :D (first working and kinda useful project)   
   
![image.png](files\image_i.png)    
   
### Zdrapek — 12/14/2023 2:22 PM   
So, I finished assembling my mechanical arm, (on the left side I still need to print the slides for the balls), I printed the main parts (blue) on my 3D printer, Creality ender 3 v3 SE.  when it comes to electronics, everything is controlled by Arduino RP2040 connect, which is connected to a 16x2 LCD display, five potentiometers (four to control the arm, and one to select the operating mode) and a button to confirm the operating mode), I chose this microcontroller because it has a built-in  microphone, wifi, and bluetooth (and because it was lying unused on the desk), the only thing left for me to do was to reprint the left element (the current one is not complete), and, of course, program it.  When I'm finished, I'll post the finished arm and write how to make and program it   
   
![image.png](files\image_0.png)    
   
### AlexTheCreator — 12/14/2023 9:30 PM   
[IMG\_4989.mov](files\img_4989.mov)    
![image.png](files\image_1s.png)    
![image.png](files\image_14.png)    
### Mattieking — 12/15/2023 8:21 AM   
Its far from done, but its for a selfmade farmbot   
![image.png](files\image_5.png)    
   
### JankPickle — 12/15/2023 4:05 PM   
Working on my final project/first off book for my embedded controllers class, it takes a modified osu!mania 4k map file and it runs it, not done yet but this was the most intimidating part getting it to render as it's supposed to, hold notes aren't implemented yet but I've got a data structure for them implemented. I have a way to read input that I need to connect fully to the game, not sure how many songs I can actually fit yet but there is a system to support more. This is an incredibly easy song so I'm not sure how well it scales up yet but with the limitations of the screen probably not very well. I have an MP3 player to attach so hopefully it syncs ok lol   
[PXL\_20231215\_194605053.TS.mp4](files\pxl_20231215_194605053-ts.mp4)    
   
### MilesTailsPrower — 12/17/2023 6:12 PM Couldent capture video   
Check out this Arduino alarm system thing I made!   
That 777 DC Motor is just an example of what you could trigger when the alarm goes off. Uses light sensor as the alarm sensor.   
![image.png](files\image_e.png)    
   
### Max — 12/19/2023 3:17 AM   
been busy with Arduino at school   
[servo.mp4](files\servo.mp4)    
   
![23203907fa019a4a4444dcc1dfeeced5.webp](files\23203907fa019a4a4444dcc1dfeeced5.webp)    
### Kanken6174 — 12/19/2023 4:21 PM   
gross tileable keypad prototype, it works…   
![image.png](files\image_v.png)    
   
### Bearkirb — 12/19/2023 4:43 PM   
Sinusoidal stepper motor! Perfect circles soon with xy plotter   
[IMG\_0773.mov](files\img_0773.mov)    
   
### Shaman — 12/20/2023 4:05 AM YT video   
[https://youtu.be/bcLq2DPabHc](https://youtu.be/bcLq2DPabHc)
Getting weather data over wifi   
![image.png](files\image_r.png)    
   
### tehnics — 12/21/2023 2:57 PM   
Hi. I made an automation based on DWIN screen and Mega2560.
If need info, just DM me   
![image.png](files\image_p.png)    
   
### JankPickle — 12/22/2023 6:25 AM   
This is where I got to before I had to submit, music was working before I broke the solder joints lol   
![image.png](files\image_4.png)    
   
### Mega\_Radon — 12/22/2023 12:42 PM    
No idea what it is?   
![image.png](files\image_1.png)    
   
### Brode — 12/23/2023 6:10 AM   
[https://youtu.be/-TrfSezAbA8?si=f-l-W73cIbCSX105](https://youtu.be/-TrfSezAbA8?si=f-l-W73cIbCSX105)   
![image.png](files\image_f.png)    
   
### big eddy spaghetti — 12/23/2023 9:58 AM   
[IMG\_0296.mov](files\img_0296.mov)    
   
### heeck69 — 12/29/2023 3:50 AM   
Distance measurement using LCD screen and sonic sensor
Yes I'm still a beginner lol
(Ignore my desk)(edited)   
![image.png](files\image_6.png)    
![image.png](files\image_q.png)    
![image.png](files\image_w.png)    
   
### dolce\_st — 12/29/2023 7:18 AM   
Spotify controller (has buttons but requires premium for control)   
![image.png](files\image_26.png)    
![image.png](files\image_u.png)    
   
### Elmi Uunsi — 12/29/2023 8:40 AM   
Ignore the tape   
[72488434438\_\_C409AEE4-692E-46D6-908D-B0ECBE2B3B84.mov](files\72488434438__c409aee4-692e-46d6-908d-b0ecbe2b3b8.mov)    
   
## Kurticus — 12/29/2023 2:04 PM   
This is the first assembly of my attempted calculator build. Baring the inevitable teardown to fix something stupid, we are on to software!   
![image.png](files\image_c.png)    
   
### Bexin — 12/29/2023 8:32 PM   
I am slowly but surely making my own simple graphics engine for arduino giga, I now have working triangles and squares rasterization by directly writing to arrays with pixel data.   
![image.png](files\image_1p.png)    
   
### Lvicek07 — 12/30/2023 5:19 AM   
[VID20231230102412.mp4](files\vid20231230102412.mp4)    
   
### maniek86 — 12/30/2023 7:20 AM   
Audio amplifier with Arduino Mega, Case is from old network switch, reused the front LEDs in a creative way. Includes FM radio, Bluetooth and AUX input.  20231229\_202611.mp4   
[20231229\_202611.mp4](files\20231229_202611.mp4)    
   
### Elmi Uunsi — 12/30/2023 4:15 PM   
Discord won’t let me post a large vid so here is a small clip of a project I’ve been working on for the last few days. (It’s supposed to get faster and harder as your score gets higher, but I’m really bad at the game so I haven’t reached that far)   
[IMG\_1888.mov](files\img_1888.mov)    
   
### Kanken6174 — 12/30/2023 4:53 PM   
this is a long shot from arduino… but here's my mipi csi to usb UVC board finally working after 5 months of headaches X')   
![image.png](files\image_g.png)    
